# doc-ui-elp_elementPlus的二次封装的文档

> 源码目录：https://naturefw.gitee.io/  

#### 介绍
基于 elementPlus 的二次封装的帮助文档

#### 技术栈
* vite: ^2.7.0
* vue: ^3.2.23 
* axios: ^0.25.0 获取json格式的配置和文档
* element-plus: ^2.0.2 UI库
* nf-ui-elp": ^0.1.0 二次封装的UI库
* @element-plus/icons-vue: ^0.2.4 图标
* @kangc/v-md-editor:"^2.3.13 md 编辑器
* vite-plugin-prismjs: ^0.0.8 代码高亮
* nf-state": ^0.2.4 状态管理
* nf-web-storage": ^0.2.3 访问 indexedDB


#### 安装教程

1.  yarn
2.  yarn add @kangc/v-md-editor@next
3.  yarn add vite-plugin-prismjs --dev
4.  yarn dev

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

介绍
安装

基础控件
表单控件



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
