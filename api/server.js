﻿// 引入服务
const http = require('http')

// 获取上传的body
const getBody = (req) => {
  return new Promise((resolve, reject) => {
    let body = ''
    req.on('data', chunk => {
      body += chunk
    })
    req.on('end', () => {
      // 输出body，转换成对象
      try {
        if (body === '') {
          resolve({})
        } else {
          const obj = JSON.parse(body)
          resolve(obj)
        }
      } catch {
        reject({
          msg: 'json格式不正确！',
          body
        })
      }
    })
  })
}

http.createServer((req, res) => {
  // 设置响应头
  res.writeHeader(200, {
    "Content-Type" : "application/json"
  })
  // 输出开头
  // res.write('{"code":"200" ')
  
  // 解构获取 URL、请求方法和头部对象
  const { url, method, headers } = req
  console.log('\n有访问者', url, method)
  
  // 请求体
  getBody(req).then(body => {
    // 需要转换成对象。
    // console.log('\n获取body', body)

    // 判断url，加载对应的服务
    const arr = url.split('/')
    if (arr.length >= 3) {
      // 符合 /api/moduleId/actionId/dataid 的形式
      const moduleId = arr[2]
      const actionId = arr[3]
      const dataId = arr.length > 2 ? arr[4]: ''
      switch (arr[1]) {
        case 'api':
          // 加载处理程序
          const service = require('./save-navi.js')
          // console.log('\n获取body2', moduleId, file)
          // 写入文件
          service[moduleId](body).then(data => {
            const re = {
              code: 0, // 成功
              data: data
            }
            res.write(JSON.stringify(re, null, 2))
            res.end()
          }).catch(err => {
            const re = {
              code: 1, // 保存导航失败
              err: err
            }
            res.write(JSON.stringify(re, null, 2))
            res.end()
          })
          
        break
      }
    }
  }).catch(err => {
    console.log(err, err.msg, err.body)
    const re = {
      code: 600 // 出错了用啥编号
    }
    res.write(JSON.stringify(re, null, 2))
    res.end()
  })
  
  
})
// 设置监听端口为 6000
.listen(6000, () => {
  console.log('服务已经开启：http://127.0.0.1:6000')
})
