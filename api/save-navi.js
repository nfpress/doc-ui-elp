var fs = require('fs')
const path = require('path')

const getPath = (file) => path.resolve(__dirname, file)

const dicFile = {
  navi: (f) => getPath(`../public/docs/.nfpress/${f}`),
  menu: (f) => getPath(`../public/docs/.nfpress/${f}`),
  doc: (f) => getPath(`../public/docs/guide/${f}`)
}

// 第一个参数：文件路径
// 第二个参数：文件内容
// 第三个参数：回调函数
//    成功：
//      文件写入成功
//      error 是 null
//    失败：
//      文件写入失败
//      error 就是错误对象

/**
 * 保存文件
 * @param {*} _path 文件路径和文件名称
 * @param {*} doc 文件内容 
 * @returns 
 */
function save(_path, doc) {

  return new Promise((resolve, reject) => {
    fs.writeFile(_path, doc, function (error) {
      if (error) {
        console.log('写入失败', error)
        reject({err: error})
      } else {
        console.log('写入成功了', error)
        resolve({msg: 'ok'})
      }
    })

  })
}

/**
 * 保存导航文件
 * @param {*} file 文件名称和内容
 * * name
 * * doc
 * @returns 
 */
function navi(file) {
  const _path = dicFile.navi(file.name)
  return save(_path, file.navi)

}

/**
 * 保存菜单文件
 * @param {*} file 文件名称和内容
 * * name
 * * doc
 * @returns 
 */
function menu(file) {
  const _path = dicFile.menu(file.name)
  return save(_path, file.menu)

}


/**
 * 保存帮助文档文件
 * @param {*} file 文件名称和内容
 * * name
 * * doc
 * @returns 
 */
function doc(file) {
  const _path = dicFile.doc(file.name)
  console.log('\n doc ', _path)
  return save(_path, file.doc)

}

module.exports = {
  navi,
  menu,
  doc
}