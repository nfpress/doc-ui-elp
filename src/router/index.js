
import { createRouter } from '@naturefw/press-edit'

// 设置 axios 的 baseUrl
const baseUrl = (document.location.host.includes('.gitee.io')) ?
  '/doc-ui-elp/' :  '/'

export default createRouter({
  baseUrl,
  components: {
    base101: () => import('../../lib/base/101_text.vue')
  }
})
