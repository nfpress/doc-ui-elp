import { reactive } from 'vue'

import {
  Document,
  Close,
  Plus,
  Star,
  UserFilled,
  Loading,
  Connection,
  Edit,
  FolderOpened
} from '@element-plus/icons-vue'

const dictIcon = reactive({
  'Document': Document,
  'Close': Close,
  'Plus': Plus,
  'Star': Star,
  'UserFilled': UserFilled,
  'Loading': Loading,
  'Connection': Connection,
  'Edit': Edit,
  'FolderOpened': FolderOpened
})

const installIcon = (app) => {
  // 便于模板获取
  app.config.globalProperties.$icon = dictIcon
}

export default installIcon