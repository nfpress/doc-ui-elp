import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

// 轻量级状态
// 设置 indexedDB 数据库，存放文档的各种信息。
import { setupIndexedDB, setupStore } from '@naturefw/press-edit'
// 初始化 indexedDB 数据库
setupIndexedDB()
  
// UI库
// import ElementPlus from 'element-plus'
// import 'element-plus/lib/theme-chalk/index.css'
// import 'dayjs/locale/zh-cn'
// import zhCn from 'element-plus/es/locale/lang/zh-cn'

// 二次封装
import { nfElementPlus, dialogDrag } from '@naturefw/ui-elp'
// 设置icon
import installIcon from './icon/index.js'

// 设置 Markdown 的配置函数
import setMarkDown from './main-md.js'

// 主题
import vuepressTheme from '@kangc/v-md-editor/lib/theme/vuepress.js'

const {
  VueMarkdownEditor, // Markdown 的编辑器
  VMdPreview // Markdown 的浏览器
} = setMarkDown(vuepressTheme)

const app = createApp(App)

app.use(setupStore) // 状态管理
  .use(router) // 需要加载组件的路由
  // .use(nfElementPlus) // 二次封装的组件
  .use(dialogDrag)
  .use(installIcon) // 注册全局图标
  // .use(ElementPlus, { locale: zhCn, size: 'small' }) // UI库
  .use(VueMarkdownEditor) // markDown编辑器
  .use(VMdPreview) // markDown 显示
  .mount('#app')
