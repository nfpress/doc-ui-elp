import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path' 

// md 编辑器需要的 代码高亮功能
import prismjs from 'vite-plugin-prismjs'


// 库模式
const lib = defineConfig({
  plugins: [
    vue(),
    prismjs({
      languages: ['json', 'js', 'html', 'sql', 'xml'], // all
    })
  ],
  // 打包配置
  build: {
    lib: {
      entry: resolve(__dirname, 'lib/main.js'),
      name: 'nfDocUIElp',
      format: ['es'],
      fileName: (format) => `doc-ui-elp.${format}.js`
    },
    sourcemap: true,
    rollupOptions: {
      // 确保外部化处理那些你不想打包进库的依赖
      external: [
        'vue',
        'axios',
        '@naturefw/nf-tool',
        '@naturefw/nf-state',
        '@naturefw/storage',
        '@naturefw/ui-core',
        '@naturefw/ui-elp',
        'element-plus',
        '@element-plus/icons-vue',
        '@kangc/v-md-editor',
      ],
      output: {
        // 在 UMD 构建模式下为这些外部化的依赖提供一个全局变量
        globals: {
          vue: 'Vue',
          'axios': 'axios',
          '@naturefw/nf-tool': 'nfTool',
          '@naturefw/nf-state': 'nfState',
          '@naturefw/storage': 'nfWebStorage',
          '@naturefw/ui-core': 'nfUICore',
          '@naturefw/ui-elp': 'nfUIElp',
          'element-plus': 'ElementPlus',
          '@element-plus/icons-vue': 'iconsVue'
        }
      }
    }
  }
})

// 开发模式、生产模式
const project = (url) => {
  return defineConfig({
    plugins: [
      vue(),
      prismjs({
        languages: ['json', 'js', 'html', 'sql', 'xml'], // all
      })
    ],
    resolve: {
      alias: {
        '/@': resolve(__dirname, '.', 'src')
      }
    },
    base: url,
    // 打包配置
    build: {
      sourcemap: true,
      outDir: 'distp', //指定输出路径
      assetsDir: 'static/img/', // 指定生成静态资源的存放路径
      rollupOptions: {
        output: {
          manualChunks(id) {
            if (id.includes('node_modules')) {
              const arr = id.toString().split('node_modules/')[1].split('/')
              switch(arr[0]) {
                // case '@kangc': // md 编辑器
                // case 'axios':
                case '@popperjs':
                case '@vue':
                case 'element-plus': // UI 库
                case '@element-plus': // 图标
                case '@naturefw': // 自然框架
                  return '_' + arr[0]
                  break
                default :
                  return '__vendor'
                  break
              }
            }
          },
          chunkFileNames: 'static/js1/[name]-[hash].js',
          entryFileNames: 'static/js2/[name]-[hash].js',
          assetFileNames: 'static/[ext]/[name]-[hash].[ext]'
        },
        brotliSize: false, // 不统计
        target: 'esnext', 
        minify: 'esbuild' // 混淆器，terser构建后文件体积更小
      }
    },
    // 本地运行配置，及反向代理配置
    server: {
      cors: true, // 默认启用并允许任何源
      open: true, // 在服务器启动时自动在浏览器中打开应用程序
      // 反向代理配置，注意rewrite写法，开始没看文档在这里踩了坑
      proxy: {
        '/api': {
          target: 'http://127.0.0.1:6000/', // 代理接口
          // rewrite: (path) => path // .replace(/^\/api/, ''),
          changeOrigin: true
        }
      }
    }
  })
}


export default ({ mode }) => {
  const url = loadEnv(mode, process.cwd()).VITE_BASEURL
  switch (url) {
    case 'lib': // 打包库文件
      return lib
      break;
    
    default: // 开发模式、生产模式
      return project(url)
      break;
  }
}